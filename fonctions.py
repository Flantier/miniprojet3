import sqlite3
import os
import csv
import logging



'''
logging
'''
def creationFichierLogging():
    '''
    Creer le fichier ou seront visibles les logging
    '''
    logging.basicConfig(filename="loggingCSVtoSQL.log",level=logging.DEBUG,format='%(asctime)s %(message)s')

def gestionLogging(message):
    '''
    Permet d'ecrire les differents logging du projet
    '''
    logging.info(message)


'''
gestion csv
'''
def ouvertureCSV(fichier):
    '''
    Permet l'ouverture d'un fichier CSV
    fichier : le fichier CSV que l'on souhaite ouvrir
    '''
    return open(fichier, newline='')

def ouvertureDictionnaire(csvfile,fieldname, delimiteur):
    '''
    à partir d'un fichier csv, permet de créer son dictionnaire en ouverture
    csvfile : le lecteur CSV lié au CSV
    fieldname : les différents attributs permettant le tri du fichier CVS
    delimiteur : caractère séparant chaque valeur du fichier
    '''
    return csv.DictReader(csvfile, fieldnames=fieldname, delimiter=delimiteur)



'''
gestion sql
'''
def gestionSQL(nomBase):
    '''
    permet de gestionner la création d'une base de données et d'une table si nécessaire
    nomBase : nome de la base à vérifier
    '''
    return sqlite3.connect(nomBase + '.db')

def setCursor(connection):
    '''
    permet créer le curseur lié à la base de données
    connection : le connecteur à la table SQL
    '''
    return connection.cursor()

def createTable(cursor):
    '''
    Permet de crée la table SIV utilisée dans ce miniprojet
    cursor : curseur lié à une base de données
    reader :stock ce qui est écrit dans le file
    '''
    with open("createTable.txt") as file:
        reader = file.read()
        cursor.execute(reader)


'''
csv to sql
'''
def csvToSql(cursor,fieldname,reader):
    '''
    Permet la conversion de données dans un fichier csv vers une table SQL
    cursor : curseur lié à une base de données
    fieldname : liste représentant les différents noms des colonnes de la table à implémenter
    reader: le DictReader du fichier en lecture
    '''
    liste = []
    for row in reader:
        for c in range (0,19):
            liste.append(row[fieldname[c]])

        immatriculation = (liste[3],)
        cursor.execute('SELECT immatriculation FROM SIV WHERE immatriculation=?', immatriculation)
        resultat = cursor.fetchone()
        if ( resultat == liste[3] ):
            majLigneSql(cursor,fieldname, liste)
        else:
            creationLigneSql(cursor,fieldname,liste)
        for c in range (0,19):
            del liste[0]
        
def majLigneSql(cursor,fieldname,liste):
    '''
    Permet la mise à jour d'une ligne de tableau SQL
    cursor : curseur lié à une base de données
    fieldname : liste représentant les différents noms des colonnes de la table à implémenter
    liste : liste des valeurs à entrer dans une ligne du tableau
    '''
    cursor.execute('UPDATE SIV SET addresse_titulaire=? WHERE immatriculation=?', (liste[0],liste[3],))
    cursor.execute('UPDATE SIV SET nom=? WHERE immatriculation=?', (liste[1],liste[3],))
    cursor.execute('UPDATE SIV SET prenom=? WHERE immatriculation=?', (liste[2],liste[3],))
    cursor.execute('UPDATE SIV SET immatriculation=? WHERE immatriculation=?', (liste[3],liste[3],))
    cursor.execute('UPDATE SIV SET date_immatriculation=? WHERE immatriculation=?', (liste[4],liste[3],))
    cursor.execute('UPDATE SIV SET vin=? WHERE immatriculation=?', (liste[5],liste[3],))
    cursor.execute('UPDATE SIV SET marque=? WHERE immatriculation=?', (liste[6],liste[3],))
    cursor.execute('UPDATE SIV SET denomination_commerciale=? WHERE immatriculation=?', (liste[7],liste[3],))
    cursor.execute('UPDATE SIV SET couleur=? WHERE immatriculation=?', (liste[8],liste[3],))
    cursor.execute('UPDATE SIV SET carrosserie=? WHERE immatriculation=?', (liste[9],liste[3],))
    cursor.execute('UPDATE SIV SET categorie=? WHERE immatriculation=?', (liste[10],liste[3],))
    cursor.execute('UPDATE SIV SET cylindre=? WHERE immatriculation=?', (liste[11],liste[3],))
    cursor.execute('UPDATE SIV SET energie=? WHERE immatriculation=?', (liste[12],liste[3],))
    cursor.execute('UPDATE SIV SET places=? WHERE immatriculation=?', (liste[13],liste[3],))
    cursor.execute('UPDATE SIV SET poids=? WHERE immatriculation=?', (liste[14],liste[3],))
    cursor.execute('UPDATE SIV SET puissance=? WHERE immatriculation=?', (liste[15],liste[3],))
    cursor.execute('UPDATE SIV SET type=? WHERE immatriculation=?', (liste[16],liste[3],))
    cursor.execute('UPDATE SIV SET variante=? WHERE immatriculation=?', (liste[17],liste[3],))
    cursor.execute('UPDATE SIV SET poids=? WHERE immatriculation=?', (liste[17],liste[3],))



def creationLigneSql(cursor,fieldname,liste):
    '''
    Permet la création d'une ligne dans un tableau SQL
    cursor : curseur lié à une base de données
    fieldname : liste représentant les différents noms des colonnes de la table à implémenter
    liste : liste des valeurs à entrer dans une ligne du tableau
    '''
    cursor.execute('INSERT OR REPLACE INTO SIV (immatriculation) VALUES (?)',(liste[3],) )
    majLigneSql(cursor,fieldname,liste)



'''
fermeture CSV et commit SQL
'''
def finalisation(csvfile,connection):
    '''
    Permet la fermeture d'un lecteur de fichier CSV, ainsi que le commit des données SQL
    csvfile : le lecteur CSV
    connection : le connecteur à la table SQL
    '''
    csvfile.close()
    connection.commit()