import unittest
import sqlite3
import csv
import os
import logging
from fonctions import ouvertureCSV,ouvertureDictionnaire,gestionSQL,setCursor,csvToSql,majLigneSql,creationLigneSql,finalisation,creationFichierLogging,gestionLogging

class TestCSVToSQLFunctions(unittest.TestCase):

    '''
    tests CSV
    '''
    def test_ouvertureCSV(self):
        fichier = 'autoV2.csv'
        with open(fichier, newline='') as test_r:
            self.assertEqual(ouvertureCSV(fichier),test_r)

    def test_ouvertureDictionnaire(self):
        fichier = 'autoV2.csv'
        delimiteur='*'
        fieldname = ['addresse_titulaire', 'nom', 'prenom', 'immatriculation', 'date_immatriculation', 'vin', 'marque',
        'denomination_commerciale', 'couleur', 'carrosserie', 'categorie', 'cylindre', 'energie', 'places', 'poids', 
        'puissance', 'type', 'variante', 'version']
        test_r = ouvertureCSV(fichier)
        dict_r = csv.DictReader(test_r, fieldnames=fieldname, delimiter=delimiteur)
        self.assertEqual(ouvertureDictionnaire(test_r,fieldname,delimiteur),dict_r)


    '''
    tests logging
    '''
    def test_ecritureLogging(self):
        creationFichierLogging()
        gestionLogging('test\n')
        with open("loggingCSVtoSQL.log") as file:
            data=file.read()
            self.assertNotEqual(data,'')

