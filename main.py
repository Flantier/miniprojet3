import sqlite3
import os
import csv
import logging
from fonctions import *

fieldname = ['addresse_titulaire', 'nom', 'prenom', 'immatriculation', 'date_immatriculation', 'vin', 'marque',
'denomination_commerciale', 'couleur', 'carrosserie', 'categorie', 'cylindre', 'energie', 'places', 'poids', 
'puissance', 'type', 'variante', 'version']

'''
mise en place du fichier de logging
'''
creationFichierLogging()

'''
mise en place du CSV
'''
csvfile = ouvertureCSV('autoV2.csv')
gestionLogging('Fichier CSV ouvert')

dicReader = ouvertureDictionnaire(csvfile,fieldname,'*')
gestionLogging('Dictionnaire du CSV crée')

'''
mise en place du SQL
'''
connection = gestionSQL('csvtosql')
cursor = setCursor(connection)
gestionLogging('Ouverture/création de la base de données')
createTable(cursor)
gestionLogging('Ouverture/création du tableau')

'''
envoie des données du CSV au SQL
'''
csvToSql(cursor,fieldname,dicReader)
gestionLogging('Ecriture du fichier CSV vers la table SQL terminé')

'''
fermeture CSV et commit SQL
'''
finalisation(csvfile,connection)
gestionLogging('Fermeture du fichier et commit SQL, fin du programme')
